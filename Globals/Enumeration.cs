﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmitCommonLibrary.Globals
{
    public class Enumeration
    {
        public enum LogLevel
        {
            Trace = 0,
            Debug = 1,
            Information = 2,
            Warning = 3,
            Error = 4,
            Critical = 5
        }
        public enum LogTarget
        {
            File,
            Database,
            EventLog
        }
        public enum RequestType
        {
            GET,
            POST,
            PUT,
            DELETE,
            HEAD,
            PATCH,
            OPTIONS,
            FORM,
            QUERYSTRING
        }
       
    }
}