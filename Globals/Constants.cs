﻿
namespace AmitCommonLibrary.Globals
{
    public static class Constants
    {
        public static class HttpStatusCodes
        {
            public const string ContinueRequest = "100";
            public const string SwitchingProtocols = "101";
            public const string Processing = "102";
            public const string OK = "200";
            public const string Created = "201";
            public const string Accepted = "202";
            public const string NonAuthoritativeInformation = "203";
            public const string NoContent = "204";
            public const string ResetContent = "205";
            public const string PartialContent = "206";
            public const string MultiStatus = "207";
            public const string AlreadyReported = "208";
            public const string IMUsed = "226";
            public const string MultipleChoices = "300";
            public const string MovedPermanently = "301";
            public const string Found = "302";
            public const string SeeOther = "303";
            public const string NotModified = "304";
            public const string UseProxy = "305";
            public const string TemporaryRedirect = "307";
            public const string PermanentRedirect = "308";
            public const string BadRequest = "400";
            public const string Unauthorized = "401";
            public const string PaymentRequired = "402";
            public const string Forbidden = "403";
            public const string NotFound = "404";
            public const string MethodNotAllowed = "405";
            public const string NotAcceptable = "406";
            public const string ProxyAuthenticationRequired = "407";
            public const string RequestTimeout = "408";
            public const string Conflict = "409";
            public const string Gone = "410";
            public const string LengthRequired = "411";
            public const string PreconditionFailed = "412";
            public const string PayloadTooLarge = "413";
            public const string UriTooLong = "414";
            public const string UnsupportedMediaType = "415";
            public const string RangeNotSatisfiable = "416";
            public const string ExpectationFailed = "417";
            public const string MisdirectedRequest = "421";
            public const string UnprocessableEntity = "422";
            public const string Locked = "423";
            public const string FailedDependency = "424";
            public const string Unassigned = "425";
            public const string UpgradeRequired = "426";
            public const string PreconditionRequired = "428";
            public const string TooManyRequests = "429";
            public const string RequestHeaderFieldsTooLarge = "431";
            public const string UnavailableForLegalReasons = "451";
            public const string InternalServerError = "500";
            public const string NotImplemented = "501";
            public const string BadGateway = "502";
            public const string ServiceUnavailable = "503";
            public const string GatewayTimeout = "504";
            public const string HttpVersionNotSupported = "505";
            public const string VariantAlsoNegotiates = "506";
            public const string InsufficientStorage = "507";
            public const string LoopDetected = "508";
            public const string NotExtended = "510";
            public const string NetworkAuthenticationRequired = "511";

        }
        public static class Api
        {
            public const string Version = "v1";
            public const string ApiDetails = "ApiDetails";
            public const string AppSettingsJSON = "appsettings.json";
            public const string CorsPolicy = "CorsPolicy";
            public const string ContentType = "application/json";
        }
        public static class JSONPropertyNames
        {
            //property names
            public const string Id = "id";
            public const string UserName = "user_name";
            public const string Password = "password";
            public const string EntryDate = "entry_date";
            public const string ModifyDate = "modify_date";
            public const string MobileNumber = "mobile_number";
            //property names

        }
        public static class UserMessages
        {
            public const string MobileNumberRequired = "Mobile number is required";
            public const string PasswordRequired = "Password is required";
            public const string UsernamePasswordBothRequired = "Username and Password are required";
            public const string UserDataRequired = "All required fields are needed";
            public const string UserSaved = "New user created successfully";
            public const string UserAuthorized = "User authorized, username and password matches";
            public const string UserNotFound = "User not found";
            public const string UserExist = "User already exists";
            public const string UserPasswordHashFail = "Password does not match";
            public const string UserPasswordMissing = "System error while matching password";
            public const string NoPermissionToAddUser = "No permission to add new user";
            public const string FileDownloadedSuccessfully = "File downloaded successfully";
            public const string AppGuidRequired = "App guid is required";
            public const string ApplicationAccessDenied = "Application access denied";
          
            public const string TokenError = "Error in token generation";
            public const string OperationSuccessfull = "Operation Successfull";
        }
        public static class LogLevel
        {
            public const string VERBOSE = "verbose";
            public const string TRACE = "trace";
            public const string INFORMATION = "information";
            public const string DEBUG = "debug";
            public const string WARNING = "warning";
            public const string ERROR = "error";
            public const string CRITICAL = "critical";
            public const string FATAL = "fatal";
            public const string NONE = "none";
        }
    }
}