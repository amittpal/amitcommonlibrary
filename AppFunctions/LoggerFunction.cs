﻿using AmitCommonLibrary.Globals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmitCommonLibrary.AppFunctions
{
    public static class LoggerFunction
    {
        /// <summary>
        /// Log exception or errors in a physical file
        /// </summary>
        /// <param name="_error"></param>
        /// <param name="_methoName"></param>
        /// <param name="_lineNumner"></param>
        /// <param name="_className"></param>
        /// <param name="_logLevel"></param>
        public static void SeriLogAsync(string _error, string _methoName, double? _lineNumner, string? _className, Enumeration.LogLevel? _logLevel = Enumeration.LogLevel.Error)
        {
            Task.Run(() => SeriLog(_error, _methoName, _lineNumner, _className, _logLevel));
        }


        /// <summary>
        /// Log exception or errors in a physical file
        /// </summary>
        /// <param name="_error"></param>
        /// <param name="_methodName"></param>
        /// <param name="_lineNumner"></param>
        /// <param name="_className"></param>
        /// <param name="_logLevel"></param>
        private static bool SeriLog(string _error, string _methodName, double? _lineNumner, string? _className, Enumeration.LogLevel? _logLevel)
        {
            bool _flag = true;
            try
            {
                _error = $"[{_className}] [{_methodName}] [{_lineNumner}] : {_error}";
                switch (_logLevel.ToString().ToLower())
                {
                    case Constants.LogLevel.VERBOSE:
                        Serilog.Log.Logger.Verbose(_error);
                        break;
                    case Constants.LogLevel.TRACE:
                        Serilog.Log.Logger.Verbose(_error);
                        break;
                    case Constants.LogLevel.INFORMATION:
                        Serilog.Log.Logger.Information(_error);
                        break;
                    case Constants.LogLevel.DEBUG:
                        Serilog.Log.Logger.Debug(_error);
                        break;
                    case Constants.LogLevel.WARNING:
                        Serilog.Log.Logger.Warning(_error);
                        break;
                    case Constants.LogLevel.ERROR:
                        Serilog.Log.Logger.Error(_error);
                        break;
                    case Constants.LogLevel.CRITICAL:
                        Serilog.Log.Logger.Fatal(_error);
                        break;
                    case Constants.LogLevel.FATAL:
                        Serilog.Log.Logger.Fatal(_error);
                        break;
                    case Constants.LogLevel.NONE:
                        break;
                    default:
                        break;
                }
            }
            catch { _flag = false; }
            return _flag;
        }
    }
}
