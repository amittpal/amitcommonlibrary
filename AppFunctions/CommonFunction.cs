﻿using AmitCommonLibrary.Globals;
using AmitCommonLibrary.Models;
using AmitCommonLibrary.Models.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace AmitCommonLibrary.AppFunctions
{
    public static class CommonFunction
    {
        private static IFunctionReturn _functionReturn;
        private static string _jsonReturn = string.Empty;
        private static IApiResponse _apiResponse;

        /// <summary>
        /// Generate random number between 1000 - 999999
        /// </summary>
        /// <returns></returns>
        public static int GenerateRandomDigit() => new Random().Next(1000, 999999);

        /// <summary>
        /// Validate User Login Input
        /// </summary>
        /// <param name="_userLogin"></param>
        /// <returns></returns>
        public static (string _jsonReturn, IFunctionReturn _functionReturn) ValidateUserLoginInput(UserLogin _userLogin)
        {
            _jsonReturn = string.Empty;
            _functionReturn = new FunctionReturn();
            if (_userLogin == null)
                _jsonReturn = Constants.UserMessages.UserDataRequired;

            else if (string.IsNullOrEmpty(_userLogin.MobileNumber) && string.IsNullOrEmpty(_userLogin.Password))
                _jsonReturn = Constants.UserMessages.UsernamePasswordBothRequired;

            else if (string.IsNullOrEmpty(_userLogin.MobileNumber))
                _jsonReturn = Constants.UserMessages.MobileNumberRequired;

            else if (string.IsNullOrEmpty(_userLogin.Password))
                _jsonReturn = Constants.UserMessages.PasswordRequired;

            if (!string.IsNullOrEmpty(_jsonReturn))
                return (_jsonReturn, _functionReturn) = CommonFunction.AppError(_jsonReturn, MethodBase.GetCurrentMethod().Name);
            else
                return (_jsonReturn, _functionReturn) = CommonFunction.AppSuccess(null, _jsonReturn, MethodBase.GetCurrentMethod().Name);
        }

        /// <summary>
        /// Returns App Error
        /// </summary>
        /// <param name="_errorMessage"></param>
        /// <param name="_methodName"></param>
        /// <returns></returns>
        public static (string _jsonReturn, IFunctionReturn _functionReturn) AppError(string _errorMessage, string _methodName)
        {
            _functionReturn = new FunctionReturn
            {
                Status = false,
                HttpStatusCode = Constants.HttpStatusCodes.Unauthorized,
                Message = new List<string>
                {
                    _errorMessage
                },
                MethodName = _methodName
            };
            _apiResponse = new ApiResponse
            {
                HttpStatusCode = Constants.HttpStatusCodes.Unauthorized,
                Message = _errorMessage
            };
            _jsonReturn = JsonConvert.SerializeObject(_apiResponse, Formatting.None);
            return (_jsonReturn, _functionReturn);
        }

        /// <summary>
        /// Returns App Success
        /// </summary>
        /// <param name="_successMessage"></param>
        /// <param name="_methodName"></param>
        /// <returns></returns>
        public static (string _jsonReturn, IFunctionReturn _functionReturn) AppSuccess(object? _obj, string _successMessage, string _methodName)
        {
            _functionReturn = new FunctionReturn
            {
                Status = true,
                HttpStatusCode = Constants.HttpStatusCodes.OK,
                Message = new List<string>
                {
                    Constants.UserMessages.OperationSuccessfull
                },
                MethodName = _methodName
            };
            _apiResponse = new ApiResponse
            {
                HttpStatusCode = Constants.HttpStatusCodes.OK,
                Message = Constants.UserMessages.OperationSuccessfull,
                Data = _obj
            };
            _jsonReturn = JsonConvert.SerializeObject(_apiResponse, Formatting.None);
            return (_jsonReturn, _functionReturn);
        }

        /// <summary>
        /// Log exception error
        /// </summary>
        /// <param name="_errorMessage"></param>
        /// <param name="_methodName"></param>
        /// <param name="_lineNumner"></param>
        /// <param name="_className"></param>
        /// <param name="_logLevel"></param>
        /// <returns></returns>
        public static (string _jsonReturn, IFunctionReturn _functionReturn) LogExceptionError(string _errorMessage, string _methodName, double? _lineNumner, string? _className, Enumeration.LogLevel? _logLevel = Enumeration.LogLevel.Error)
        {
            LoggerFunction.SeriLogAsync(_errorMessage, _methodName, _lineNumner, _className, _logLevel);

            _functionReturn = new FunctionReturn()
            {
                Status = false,
                HttpStatusCode = Constants.HttpStatusCodes.InternalServerError,
                Message = new List<string>
                {
                    _errorMessage
                },
                MethodName = _methodName
            };
            _apiResponse = new ApiResponse
            {
                HttpStatusCode = Constants.HttpStatusCodes.InternalServerError,
                Message = _errorMessage
            };
            _jsonReturn = JsonConvert.SerializeObject(_apiResponse, Formatting.None);
            return (_jsonReturn, _functionReturn);
        }
    }
}