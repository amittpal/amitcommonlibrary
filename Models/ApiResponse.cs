﻿using AmitCommonLibrary.Models.Interfaces;

namespace AmitCommonLibrary.Models
{
    public class ApiResponse : IApiResponse
    {
        public string HttpStatusCode { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}