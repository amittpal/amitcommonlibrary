﻿using AmitCommonLibrary.Models.Interfaces;
using Newtonsoft.Json;
using System;
using System.ComponentModel;

namespace AmitCommonLibrary.Models
{
    public class UserLogin : IUserLogin
    {
        public string Id { get; set; }
        public string MobileNumber { get; set; }
        public string Password { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}