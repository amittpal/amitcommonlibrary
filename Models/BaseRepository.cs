﻿using AmitCommonLibrary.AppFunctions;
using AmitCommonLibrary.AppFunctions.Interfaces;
using AmitCommonLibrary.Globals;
using AmitCommonLibrary.Models.Interfaces;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AmitCommonLibrary.Models
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        protected readonly IMongoDBContext _mongoContext;
        protected IMongoCollection<TEntity> _dbCollection;

        public BaseRepository(IMongoDBContext context)
        {
            _mongoContext = context;
            _dbCollection = _mongoContext.GetCollection<TEntity>(typeof(TEntity).Name);
        }


        public async Task<TEntity> Get(string id)
        {
            try
            {
                FilterDefinition<TEntity> filter = Builders<TEntity>.Filter.Eq("_id", new ObjectId(id));

                return await _dbCollection.FindAsync(filter).Result.FirstOrDefaultAsync();
            }
            catch (MongoException ex)
            {
                LoggerFunction.SeriLogAsync(ex.Message, MethodBase.GetCurrentMethod().Name, 53, this.GetType().Name, Enumeration.LogLevel.Error);
                return null;
            }
        }


        public async Task<IEnumerable<TEntity>> Get()
        {
            try
            {
                var all = await _dbCollection.FindAsync(Builders<TEntity>.Filter.Empty);
                return await all.ToListAsync();
            }
            catch (MongoException ex)
            {
                LoggerFunction.SeriLogAsync(ex.Message, MethodBase.GetCurrentMethod().Name, 53, this.GetType().Name, Enumeration.LogLevel.Error);
                return null;
            }
        }

        public async Task InsertOneAsync(TEntity obj)
        {
            try
            {
                await _dbCollection.InsertOneAsync(obj);
            }
            catch (MongoException ex)
            {
                LoggerFunction.SeriLogAsync(ex.Message, MethodBase.GetCurrentMethod().Name, 53, this.GetType().Name, Enumeration.LogLevel.Error);
            }
        }

        public async Task<ReplaceOneResult> UpdateOneAsync(TEntity obj, string _id)
        {
            try
            {
                return await _dbCollection.ReplaceOneAsync(Builders<TEntity>.Filter.Eq("_id", new ObjectId(_id)), obj);
            }
            catch (MongoException ex)
            {
                LoggerFunction.SeriLogAsync(ex.Message, MethodBase.GetCurrentMethod().Name, 53, this.GetType().Name, Enumeration.LogLevel.Error);
                return null;
            }
        }
        public async Task<DeleteResult> Delete(string id)
        {
            try
            {
                return await _dbCollection.DeleteOneAsync(Builders<TEntity>.Filter.Eq("_id", new ObjectId(id)));
            }
            catch (MongoException ex)
            {
                LoggerFunction.SeriLogAsync(ex.Message, MethodBase.GetCurrentMethod().Name, 53, this.GetType().Name, Enumeration.LogLevel.Error);
                return null;
            }
        }
    }
}