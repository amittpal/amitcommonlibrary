﻿using AmitCommonLibrary.Globals;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using static AmitCommonLibrary.Globals.Constants;

namespace AmitCommonLibrary.Models.Interfaces
{
    public interface IUserLogin
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [JsonProperty(PropertyName = JSONPropertyNames.MobileNumber)]
        public string MobileNumber { get; set; }
        [JsonProperty(PropertyName = JSONPropertyNames.Password)]
        public string Password { get; set; }
        [JsonProperty(PropertyName = JSONPropertyNames.EntryDate)]
        public DateTime EntryDate { get; set; }
        [JsonProperty(PropertyName = JSONPropertyNames.ModifyDate)]
        public DateTime ModifyDate { get; set; }
    }
}