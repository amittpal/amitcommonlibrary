﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmitCommonLibrary.Models.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        public Task InsertOneAsync(TEntity obj);
        public Task<ReplaceOneResult> UpdateOneAsync(TEntity obj,string _id);
        public Task<DeleteResult> Delete(string id);
        public Task<TEntity> Get(string id);
        public Task<IEnumerable<TEntity>> Get();
    }
}