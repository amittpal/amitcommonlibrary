﻿using Newtonsoft.Json;
using static AmitCommonLibrary.Globals.Constants;

namespace AmitCommonLibrary.Models.Interfaces
{
    public interface IApiResponse
    {
        public string HttpStatusCode { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}