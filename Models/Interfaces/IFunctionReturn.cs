﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmitCommonLibrary.Models.Interfaces
{
    public interface IFunctionReturn
    {
        public bool Status { get; set; }
        public List<string> Message { get; set; }
        public int StatusCode { get; set; }
        public string HttpStatusCode { get; set; }
        public string MethodName { get; set; }
    }
}